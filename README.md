# About
These problems are taken from https://retosdeprogramacion.com/ by mouredev. I
will translate the questions to english and write them here as I solve the
problems.

# 00 Sintax, Variables, Data Types and Hello World

Ready to learn or review the programming language of your choice?
- Remember that all participation instructions are in the GitHub repository.

First things first... Have you already chosen a language?
- Not all languages are the same, but their fundamentals are often common.
- This first challenge will help you get familiar with the way of participating by submitting your own solutions.

EXERCISE:
- Create a comment in the code and place the URL of the official website of the programming language you have selected.
- Represent the different syntaxes for creating comments in the language (single-line, multi-line...).
- Create a variable (and a constant if the language supports it).
- Create variables representing all primitive data types of the language (strings, integers, booleans...).
- Print to the terminal the text: "Hello, [and the name of your language]!"

Easy? Don't worry, remember that this is a learning path and we must start from the beginning.

# 01 Operators and Control Flow

EXERCISE:
- Create examples using all types of operators in your language: Arithmetic,
  logical, comparison, assignment, identity, membership, bitwise... (Keep in
  mind that each language may have different ones)
- Using operations with operators of your choice, create examples representing
  all types of control structures in your language: Conditionals, loops,
  exceptions...
- You must print the result of all examples to the console.

EXTRA CHALLENGE (optional): Create a program that prints to the console all
numbers between 10 and 55 (inclusive), which are even and not divisible by 3 or
equal to 16. 

I'm sure that by carefully exploring the possibilities, you've discovered
something new.
