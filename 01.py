"""
--------------------------------------------------------------------------------
# 01 Operators and Control Flow
--------------------------------------------------------------------------------

EXERCISE:
- Create examples using all types of operators in your language: Arithmetic,
  logical, comparison, assignment, identity, membership, bitwise... (Keep in
  mind that each language may have different ones)
- Using operations with operators of your choice, create examples representing
  all types of control structures in your language: Conditionals, loops,
  exceptions...
- You must print the result of all examples to the console.

EXTRA CHALLENGE (optional): Create a program that prints to the console all
numbers between 10 and 55 (inclusive), which are even and not divisible by 3 or
equal to 16. 

I'm sure that by carefully exploring the possibilities, you've discovered
something new.
"""

# Arithmetic operators: +, -, *, /, %, **, //
print('Arithmetic operators')
print(3+5)
print(8//5)

# Assignment operators:  =, +=, -=, *=, /=, %=, //=, **=, &=, |=, ^=, >>=, <<=
print('\nAssignment operators')
x=5
print(x)
x**=5
print(x)


# Comparison operators: ==, !=, >, <, >=, <=
print('\nComparison operators')
print(5==5)
print(5>1234)


# Logical operators: and, or, not
print('\nLogical operators')
print(True and False)
print(5==123 or 5<1234)


# Identity operators: is, is not
print('\nIdentity operators')
y = x
print(x is 3125)
print(x is y)
print(x is not y)


# Membership operators: in, not in
print('\nMembership operators')
print(x in [1, 2, 3, 4, 5])
print(x not in [1, 2, 3, 4, 5])


# Bitwise operators: &, |, ^, ~, <<, >>
print('\nBitwise operators')
print(0 & 1)
print(1 | 1)

# -------Control structures------
# If
x=5
if x<5:
    print("x is less than 5")
elif x==5:
    print("x is exactly 5")
else:
    print("x is greater than 5")

# for and break
for n in range(0,10):
    if n>7:
        break

    print(n)

# while and continue
n=0
while n<100:
    if n>7:
        n=n+6
        print(n)
        continue
    print(n)
    n=n+1
    

# pass
while n<100:
    pass
    n=n+1

# match
match x:
    case 10:
        print("x=10")
    case 4 | 5:
        print("x=4 or x=5")
    case _:
        print("X=?")



#EXTRA CHALLENGE (optional): Create a program that prints to the console all
#numbers between 10 and 55 (inclusive), which are even and not divisible by 3 or
#equal to 16. 

print("\n Printing all numbers between 10 and 55 which are even and not divisible by 3 or equal to 16")
for x in range(10,56):
    if x%2==1:
        continue
    elif x%3==0:
        continue
    elif x==16:
        continue
    else:
        print(x)

