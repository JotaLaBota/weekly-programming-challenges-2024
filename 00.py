"""
--------------------------------------------------------------------------------
00 Sintax, Variables, Data Types and Hello World
--------------------------------------------------------------------------------

Ready to learn or review the programming language of your choice?
- Remember that all participation instructions are in the GitHub repository.

First things first... Have you already chosen a language?
- Not all languages are the same, but their fundamentals are often common.
- This first challenge will help you get familiar with the way of participating
  by submitting your own solutions.

EXERCISE:
- Create a comment in the code and place the URL of the official website of the
  programming language you have selected.
- Represent the different syntaxes for creating comments in the language
  (single-line, multi-line...).
- Create a variable (and a constant if the language supports it).
- Create variables representing all primitive data types of the language
  (strings, integers, booleans...).
- Print to the terminal the text: "Hello, [and the name of your language]!"

Easy? Don't worry, remember that this is a learning path and we must start from
the beginning.
"""

# https://www.python.org/

# This is a single-line comment

""" This is 
a 
multiple-line
comment
"""

my_string = "This is a string"
my_integer = 5
my_float = 5.5
my_complex = 4+1j
my_boolean = True

print("Hello, Python")
